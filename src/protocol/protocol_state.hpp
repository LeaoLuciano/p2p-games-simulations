#ifndef STATE_H
#define STATE_H

#include <stdlib.h>
#include <string>
#include <list>
#include <vector>
#include <deque>

// #include <game/game.hpp>
// #include <game/player.hpp>
// #include <game/command.hpp>
// #include "packet.hpp"



class ProtocolState {
protected:
    int num_nodes;
    static const int MAX_NODES = 100;

public:

    typedef enum {
        NONE = 0, WAIT_COMMIT, COMMITTED,
        WAIT_SYNC, SYNCED, SERVER_SYNCED,
    } State;

    static const int NUM_STATES = 6;

    ProtocolState(int num_nodes): num_nodes(num_nodes) {

    }

    virtual void count_states() = 0;


};

class NormalState : public ProtocolState {
protected:
    int _count_states[NUM_STATES];
    State node_state[MAX_NODES];

public:
    NormalState(int num_nodes): ProtocolState(num_nodes) {
        for (int i = 0; i < num_nodes; i++)
            node_state[i] = NONE;
        for (int i = 0; i < NUM_STATES; i++)
            _count_states[i] = 0;
    }

    inline void set_all_states(State state) {
        for (int i = 0; i < num_nodes; i++)
            node_state[i] = state;
    }

    inline void set_state(int id, State state) {
        node_state[id] = state;
    }

    inline void count_states() {
        for (int i = 0; i < NUM_STATES; i++)
            _count_states[i] = 0;
        for (int i = 0; i < num_nodes; i++)
            _count_states[node_state[i]]++;

        // printf("%d %d %d %d %d\n", _count_states[0], _count_states[1], 
        //     _count_states[2], _count_states[3], _count_states[4]);
    }

    inline State get_state(int id) {
        return node_state[id];
    }

    inline int count_state(State state) {
        return _count_states[state];
    }
};

class MultipleState : public ProtocolState {
protected:
    int size;
    std::deque<NormalState> round_states;

public:

    MultipleState(int num_nodes, int size): size(size),
     ProtocolState(num_nodes), round_states() {
        for(int i = 0; i < size; i++)
            round_states.push_back(NormalState(num_nodes));
    }

    void set_all_states(State state, int round) {
        round_states[round].set_all_states(state);
    }

    void set_state(int id, State state, int round) {
        round_states[round].set_state(id, state);
    }

    void count_states() {
        for (int j = 0; j < size; j++) {
            round_states[j].count_states();
        }
    }
    inline State get_state(int id, int round) {
        return round_states[round].get_state(id);
    }

    inline int count_state(State state, int round) {
        return round_states[round].count_state(state);
    }

    inline void expand() {
        size++;
        round_states.push_back(NormalState(num_nodes));
    }

    inline void pop() {
        size--;
        round_states.pop_front();
    }

    inline int get_size() {
        return size;
    }
};


#endif