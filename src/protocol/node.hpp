#ifndef NODE_HPP
#define NODE_HPP

#include <unistd.h>

#include <tuple>
#include <vector>
#include <algorithm>

#include <protocol/protocol.hpp>
#include <net/communication.hpp>
#include <game/game.hpp>
#include <game/command.hpp>
#include <game/player.hpp>

class Node {
protected:
    Game& game;
    Player& player;
    // std::vector<Packet> state;

    int frame;
    
    Packet p_recv, p_send;
    Protocol& protocol;
    Conn conn;

    std::vector<Packet> incoming_packets;
    std::vector<Packet> outgoing_packets;

    static bool compare_packets(Packet &p1, Packet &p2) {
        return p1.time > p2.time;
    }

public:
    Node(Game& game, Player& player, Protocol& protocol) :
    game(game), player(player), protocol(protocol), conn(player.get_id(), game.get_num_nodes()), 
    incoming_packets(), outgoing_packets() {
        frame = 0;
        conn.connect_all();
    };

    void update() {
        player.time = frame;
        
        std::list<Packet> packets = conn.recv();
        for(Packet p : packets) {
            p.time += player.get_delay();
            incoming_packets.push_back(p);
        }

        packets.clear();
        sort(incoming_packets.begin(), incoming_packets.end(), compare_packets);
        while(incoming_packets.size() > 0 and incoming_packets.back().time <= frame) {
            packets.push_front(incoming_packets.back());
            incoming_packets.pop_back();
        }

        protocol.parse_packets(packets);

        Command command = NONE;
        
        if(not protocol.simulation_blocked())
            command = player.new_command(frame);

        protocol.update(command);

        packets.clear();
        packets = protocol.get_new_packets();
        for(Packet p: packets) {
            p.time = frame + player.get_delay();
            p.sent_time = frame;
            outgoing_packets.push_back(p);
        }

        packets.clear();
        sort(outgoing_packets.begin(), outgoing_packets.end(), compare_packets);
        while(outgoing_packets.size() > 0 and outgoing_packets.back().time <= frame) {
            packets.push_front(outgoing_packets.back());
            outgoing_packets.pop_back();
        }

        conn.send_packets(packets);

        // printf("%d %d\n", frame, protocol.get_round());
        frame++;
    };

    inline int get_frame() {
        return frame;
    }

    ~Node() {
        delete &protocol;
    }

};



#endif