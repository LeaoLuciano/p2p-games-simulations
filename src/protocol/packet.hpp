#ifndef PACKET_H
#define PACKET_H

#include <stdlib.h>

typedef struct {
    typedef enum {
        SYNC, COMMIT, NO_SYNC,
    } PacketType;
    
    u_int8_t type = 0;
    u_int16_t content = 0;
    u_int64_t content2 = 0;
    u_int8_t source = 0, dest = 0;
    u_int16_t round = 0;
    u_int16_t time = 0;
    u_int16_t sent_time = 0;

} Packet;

#endif