#ifndef RACS_H
#define RACS_H

#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <list>

#include "../protocol.hpp"

class Server : public Protocol {
protected:
    NormalState state;
    Command commands[MAX_NODES];

    void update_state() {
        
        if(state.count_state(ProtocolState::SYNCED) == game.get_num_players()) {
            state.set_all_states(ProtocolState::WAIT_SYNC);
            round++;
            return;
        }

    }

public:
    Server(Game& game, Player& player) : state(game.get_num_nodes()), Protocol(game, player, state) {
        state.set_all_states(ProtocolState::WAIT_SYNC);
    }

    void parse_packet(Packet &p) {
        if(p.round == round+1)
        {
            next_packets.push_back(p);
            return;
        }

        switch (p.type)
        {
        case Packet::SYNC:
            if(state.get_state(p.source) != ProtocolState::WAIT_SYNC)
                broke_protocol(p.source);
            state.set_state(p.source, ProtocolState::SERVER_SYNCED);
            commands[p.source] = Command(p.content);
            break;

        default:
            break;
        }

        Protocol::parse_packet(p);



    }

    void new_control_packets() {
        for(int i : game.get_players_ids()) {
            switch(state.get_state(i)) {
            case ProtocolState::SERVER_SYNCED:
                for(int j : game.get_players_ids())
                {
                    if(j == i)
                        continue;
                    Packet p;
                    p.type = Packet::SYNC;
                    p.content = u_int8_t (commands[i]);
                    p.round = round;
                    p.source = i;
                    p.dest = j;

                    packets.push_back(p);
                }
                state.set_state(i, ProtocolState::SYNCED);
                break;
            }
        }
    }

    void new_command_packets(Command command) {}

};

class Client : public Protocol {
protected:
    NormalState state;
    static const int server_id = 0;
    
    void update_state() {

        if(state.count_state(ProtocolState::SYNCED) == game.get_num_players()) {
            state.set_all_states(ProtocolState::WAIT_SYNC);
            round++;
            block = false;
            return;
        }

    }

public:
    Client(Game& game, Player& player) : state(game.get_num_nodes()), Protocol(game, player, state) {
        state.set_all_states(ProtocolState::WAIT_SYNC);
    }

    void parse_packet(Packet &p) {
        if(p.round == round+1)
        {
            next_packets.push_back(p);
            return;
        }

        switch (p.type)
        {
        case Packet::SYNC:
            if(state.get_state(p.source) != ProtocolState::WAIT_SYNC)
                broke_protocol(p.source);
            state.set_state(p.source, ProtocolState::SYNCED);
            break;

        default:
            break;
        }

        Protocol::parse_packet(p);
    }

    void new_control_packets() {}

    void new_command_packets(Command command) {
        switch(state.get_state(player.get_id())) {
        case ProtocolState::WAIT_SYNC:
            Packet p;
            p.type = Packet::SYNC;
            p.content = command;
            p.round = round;
            p.source = player.get_id();
            p.dest = server_id;

            packets.push_back(p);
            state.set_state(player.get_id(), ProtocolState::SYNCED);
            block = true;
            break;
        }
    }


};

#endif