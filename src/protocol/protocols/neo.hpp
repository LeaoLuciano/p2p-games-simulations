#ifndef NEO_H
#define NEO_H

#include <stdlib.h>
#include <string>
#include <list>

#include "../protocol.hpp"

class Neo : public Protocol {
protected:
    MultipleState state;
    Command next_command = Command::NONE;
    int round_length, round_time;
    Command commands[MAX_NODES];
    int count_votes[MAX_NODES];

    u_int64_t votes = 0, old_votes = 0;

    void update_state() {
        
        if(player.time > round_length + round_time) {
            for(int i = 0; i < game.get_num_players(); i++)
                if(count_votes[i] >= game.get_num_players()/2)
                    player.apply_command(i, commands[i], round);

            round++;
            round_time = player.time;

            for(int i = 0; i < game.get_num_players(); i++)
            {
                commands[i] = Command::NONE;
                count_votes[i] = 0;
            }
            old_votes = votes;
            votes = 0;

            state.pop();
            state.expand();
            state.set_all_states(ProtocolState::WAIT_SYNC, 1);
        }

        // if(state.count_state(ProtocolState::SYNCED) == game.get_num_players()) {
        //     // puts("A");
        //     state.set_all_states(ProtocolState::WAIT_SYNC);
        //     return;
        // }

        // if(state.count_state(ProtocolState::SYNCED) == game.get_num_players()) {
            // printf("B %d\n", round);
            // state.set_all_states(ProtocolState::WAIT_COMMIT);
            // // round++;
            // block = false;
            // return;
        // }


    }

public:
    Neo(Game& game, Player& player, int round_length) : state(game.get_num_nodes(), 2),
            Protocol(game, player, state), round_length(round_length), round_time(player.time) {
        state.set_all_states(ProtocolState::WAIT_COMMIT, 0);
        state.set_all_states(ProtocolState::WAIT_COMMIT, 1);

        for(int i = 0; i < game.get_num_players(); i++)
        {
            commands[i] = Command::NONE;
            count_votes[i] = 0;
        }
    }

    void parse_packet(Packet &p) {

        if(p.round < round)
        {
            //next_packets.push_back(p);
            return;
        }
        switch (p.type)
        {
        case Packet::COMMIT:
            if(state.get_state(p.source, 1) != ProtocolState::WAIT_COMMIT)
            {
                next_packets.push_back(p);
                return;
                //printf("A2 %d -- %d\n", player.get_id(), state.get_state(p.source));
                //broke_protocol(p.source);
            }

            // printf("COMMIT %d -> %d\n", p.source, player.get_id());
            state.set_state(p.source, ProtocolState::COMMITTED, 1);

            break;

        case Packet::SYNC:
            if(state.get_state(p.source, 0) == ProtocolState::COMMITTED)
            {
                next_packets.push_back(p);
                return;
            }
            if(state.get_state(p.source, 0) != ProtocolState::WAIT_SYNC)
            {
                next_packets.push_back(p);
                return;
                // printf("B2 %d -- %d\n", player.get_id(), state.get_state(p.source));
                // broke_protocol(p.source);
            }

            votes |= 1 << p.source;
            //all_votes[p.source] = p.content2;
            for(int i = 0; i < game.get_num_players(); i++)
                count_votes[i] += p.content2 & (1 << i);
                
            state.set_state(p.source, ProtocolState::SYNCED, 0);
            
            break;

        default:
            break;
        }

        Protocol::parse_packet(p);
    }

    void new_control_packets() {
    }

    void new_command_packets(Command command) {

        switch (state.get_state(player.get_id(), 0))
        {
        case ProtocolState::WAIT_SYNC:

            for(int i : player.get_opponents_ids())
            {
                Packet p;
                p.type = Packet::COMMIT;
                p.round = round;
                p.source = player.get_id();
                p.dest = i;

                packets.push_back(p);
            }

            if(next_command == Command::NONE)
                for(int i : player.get_opponents_ids())
                {
                    Packet p;
                    p.type = Packet::SYNC;
                    p.content = next_command;
                    p.content2 = old_votes;
                    p.round = round-1;
                    p.source = player.get_id();
                    p.dest = i;

                    packets.push_back(p);
                }
            next_command = command;
            state.set_state(player.get_id(), ProtocolState::SYNCED, 0);
            break;
        default:
            break;
        }
    }

};


#endif