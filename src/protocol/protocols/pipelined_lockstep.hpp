#ifndef PIPELINED_LOCKSTEP_H
#define PIPELINED_LOCKSTEP_H

#include <stdlib.h>
#include <string>
#include <list>
#include <deque>

#include "../protocol.hpp"

class PipelinedLockstep: public Protocol {
protected:
    MultipleState state;
    int active_states = 0;
    std::deque<Command> next_commands;
    void update_state() {

        for (int i = 0; i < state.get_size(); i++) {
            if (state.count_state(ProtocolState::COMMITTED, i) == game.get_num_players()) {
                // puts("A");
                state.set_all_states(ProtocolState::WAIT_SYNC, i);
                return;
            }
        }

        while (state.count_state(ProtocolState::SYNCED, 0) == game.get_num_players()) {
            // printf("B %d\n", round);
            state.pop();
            state.expand();
            state.set_all_states(ProtocolState::WAIT_COMMIT, state.get_size() - 1);
            next_commands.pop_front();
            next_commands.push_back(Command::NONE);
            round++;
            block = false;
        }

    }

public:
    PipelinedLockstep(Game& game, Player& player, int pipeline_size):
        state(game.get_num_nodes(), pipeline_size), Protocol(game, player, state), 
        next_commands() {
        for (int i = 0; i < pipeline_size; i++)
        {
            state.set_all_states(ProtocolState::WAIT_COMMIT, i);
            next_commands.push_back(Command::NONE);
        }
    }

    void parse_packet(Packet& p) {

        if (p.round >= round + state.get_size())
        {
            next_packets.push_back(p);
            return;
        }
        switch (p.type)
        {
        case Packet::COMMIT:
            if (state.get_state(p.source, p.round - round) != ProtocolState::WAIT_COMMIT)
            {
                // puts("C");
                // broke_protocol(p.source);
                next_packets.push_back(p);
                return;
            }

            // printf("COMMIT %d -> %d\n", p.source, player.get_id());
            state.set_state(p.source, ProtocolState::COMMITTED, p.round - round);

            break;

        case Packet::SYNC:
            if (state.get_state(p.source, p.round - round) == ProtocolState::COMMITTED)
            {
                next_packets.push_back(p);
                return;
            }
            if (state.get_state(p.source, p.round - round) != ProtocolState::WAIT_SYNC)
            {
                // puts("D");
                // // printf("B2 %d -- %d\n", player.get_id(), node_state[p.source]);
                // broke_protocol(p.source);
                next_packets.push_back(p);
                return;
            }

            state.set_state(p.source, ProtocolState::SYNCED, p.round - round);

            break;

        default:
            break;
        }

        Protocol::parse_packet(p);
    }

    void new_control_packets() {
        for (int j = 0; j < state.get_size(); j++)
        {
            if (next_commands[j] == Command::NONE)
                continue;

            if (state.get_state(player.get_id(), j) == ProtocolState::WAIT_SYNC)
            {
                for (int i : player.get_opponents_ids())
                {
                    Packet p;
                    p.type = Packet::SYNC;
                    p.content = next_commands[j];
                    p.round = round + j;
                    p.source = player.get_id();
                    p.dest = i;

                    packets.push_back(p);
                }
                next_commands[j] = Command::NONE;
                state.set_state(player.get_id(), ProtocolState::SYNCED, j);
                break;
            }

            // if (j == state.get_size() - 1)
            // {
            //     puts("A");
            //     broke_protocol(player.get_id());
            // }
        }
    }

    void new_command_packets(Command command) {
        for (int j = 0; j < state.get_size(); j++)
        {
            if (state.get_state(player.get_id(), j) == ProtocolState::WAIT_COMMIT)
            {
                if (next_commands[j] != Command::NONE)
                    broke_protocol(player.get_id());

                for (int i : player.get_opponents_ids())
                {
                    Packet p;
                    p.type = Packet::COMMIT;
                    p.round = round + j;
                    p.source = player.get_id();
                    p.dest = i;

                    packets.push_back(p);
                }
                next_commands[j] = command;
                state.set_state(player.get_id(), ProtocolState::COMMITTED, j);

                if (j == state.get_size() - 1)
                    block = true;
                break;
            }
            if (j == state.get_size() - 1)
            {
                puts("B");
                broke_protocol(player.get_id());
            }
        }
    }
};


#endif