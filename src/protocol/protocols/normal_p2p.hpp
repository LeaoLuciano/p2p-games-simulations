#ifndef NORMAL_P2P_H
#define NORMAL_P2P_H

#include <stdlib.h>
#include <string>
#include <list>

#include "../protocol.hpp"

class NormalP2P : public Protocol {
protected:
    NormalState state;

    void update_state() {

        if(state.count_state(ProtocolState::SYNCED) == game.get_num_nodes()) {
            state.set_all_states(ProtocolState::WAIT_SYNC);
            round++;
            block = false;
            return;
        }

    }

public:
    NormalP2P(Game& game, Player& player) : state(game.get_num_nodes()), Protocol(game, player, state) {
        state.set_all_states(ProtocolState::WAIT_SYNC);
    }

    void parse_packet(Packet &p) {

        if(p.round == round+1)
        {
            next_packets.push_back(p);
            return;
        }

        switch (p.type)
        {
        case Packet::SYNC:
            //  printf("%d %d %d\n", player.get_id(), p.source, node_state[p.source]);
            if(state.get_state(p.source) != ProtocolState::WAIT_SYNC)
                broke_protocol(p.source);
            state.set_state(p.source, ProtocolState::SYNCED);
            break;

        default:
            break;
        }

        Protocol::parse_packet(p);



    }

    void new_control_packets() {}

    void new_command_packets(Command command) {
        switch(state.get_state(player.get_id())) {
        case ProtocolState::WAIT_SYNC:
            for(int i : player.get_opponents_ids())
            {
                Packet p;
                p.type = Packet::SYNC;
                p.round = round;
                p.source = player.get_id();
                p.dest = i;

                packets.push_back(p);
            }
            state.set_state(player.get_id(), ProtocolState::SYNCED);
            block = true;
            break;
        default:
            break;
        }
    }


};

#endif