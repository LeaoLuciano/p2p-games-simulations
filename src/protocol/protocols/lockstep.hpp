#ifndef LOCKSTEP_H
#define LOCKSTEP_H

#include <stdlib.h>
#include <string>
#include <list>

#include "../protocol.hpp"

class Lockstep : public Protocol {
protected:
    NormalState state;
    Command next_command = Command::NONE;

    void update_state() {
        
        if(state.count_state(ProtocolState::COMMITTED) == game.get_num_players()) {
            // puts("A");
            state.set_all_states(ProtocolState::WAIT_SYNC);
            return;
        }

        if(state.count_state(ProtocolState::SYNCED) == game.get_num_players()) {
            // printf("B %d\n", round);
            state.set_all_states(ProtocolState::WAIT_COMMIT);
            round++;
            block = false;
            return;
        }

    }

public:
    Lockstep(Game& game, Player& player) : state(game.get_num_nodes()), Protocol(game, player, state) {
        state.set_all_states(ProtocolState::WAIT_COMMIT);
    }

    void parse_packet(Packet &p) {

        if(p.round == round+1)
        {
            next_packets.push_back(p);
            return;
        }
        switch (p.type)
        {
        case Packet::COMMIT:
            if(state.get_state(p.source) != ProtocolState::WAIT_COMMIT)
            {
                next_packets.push_back(p);
                return;
                //printf("A2 %d -- %d\n", player.get_id(), state.get_state(p.source));
                //broke_protocol(p.source);
            }

            // printf("COMMIT %d -> %d\n", p.source, player.get_id());
            state.set_state(p.source, ProtocolState::COMMITTED);

            break;

        case Packet::SYNC:
            if(state.get_state(p.source) == ProtocolState::COMMITTED)
            {
                next_packets.push_back(p);
                return;
            }
            if(state.get_state(p.source) != ProtocolState::WAIT_SYNC)
            {
                next_packets.push_back(p);
                return;
                // printf("B2 %d -- %d\n", player.get_id(), state.get_state(p.source));
                // broke_protocol(p.source);
            }

            state.set_state(p.source, ProtocolState::SYNCED);
            
            break;

        default:
            break;
        }

        Protocol::parse_packet(p);
    }

    void new_control_packets() {
        switch (state.get_state(player.get_id()))
        {
        case ProtocolState::WAIT_SYNC:
            if(next_command == Command::NONE)
                break;
            for(int i : player.get_opponents_ids())
            {
                Packet p;
                p.type = Packet::SYNC;
                p.content = next_command;
                p.round = round;
                p.source = player.get_id();
                p.dest = i;

                packets.push_back(p);
            }
            next_command = Command::NONE;
            state.set_state(player.get_id(), ProtocolState::SYNCED);
            block = true;
            break;
        default:
            break;
        }
    }

    void new_command_packets(Command command) {
        switch (state.get_state(player.get_id()))
        {
        case ProtocolState::WAIT_COMMIT:
            for(int i : player.get_opponents_ids())
            {
                Packet p;
                p.type = Packet::COMMIT;
                p.round = round;
                p.source = player.get_id();
                p.dest = i;

                packets.push_back(p);
            }
            next_command = command;
            state.set_state(player.get_id(), ProtocolState::COMMITTED);
            break;
        default:
            break;
        }
        return;
    }

};


#endif