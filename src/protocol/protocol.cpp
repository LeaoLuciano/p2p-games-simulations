#include "protocol.hpp"

#include <list>
#include <iterator>
#include <algorithm>

void Protocol::cheated(int node) {}

void Protocol::cheated_detected(int node) {}

void Protocol::broke_protocol(int node) {puts("INVALID PROTOCOL STATE");}

void Protocol::parse_packets(std::list<Packet> incoming_packets) {
    copy(next_packets.rbegin(), next_packets.rend(), front_inserter(incoming_packets));
    next_packets.clear();
    
    for(Packet p : incoming_packets)
        parse_packet(p);
}

void Protocol::update(Command command) {
    _state.count_states();
    update_state();
    new_control_packets();
    if(command != NONE)
        new_command_packets(command);
    return;
}