#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <stdlib.h>
#include <string>
#include <list>

#include <game/game.hpp>
#include <game/player.hpp>
#include <game/command.hpp>
#include "packet.hpp"
#include "protocol_state.hpp"


const int MAX_NODES = 64;

class Protocol {
protected:

    Game &game;
    Player &player;
    int round;
    bool block;

    ProtocolState& _state;

    std::list<Packet> packets;
    std::list<Packet> next_packets;

    void cheated(int node);

    void cheated_detected(int node);

    void broke_protocol(int node);

    virtual void update_state() = 0;

    virtual void new_control_packets() = 0;

    virtual void new_command_packets(Command command) = 0;

    virtual void parse_packet(Packet &p) {};

public:

    Protocol(Game& game, Player &player, ProtocolState &state):
    game(game), player(player), _state(state), round(0), block(false) {
    }

    void parse_packets(std::list<Packet> incoming_packets);

    void update(Command command);

    inline int get_round() {
        return round;
    }

    inline std::list<Packet> get_new_packets() {
        std::list<Packet> ps(packets);
        packets.clear();
        return ps;
    }

    bool simulation_blocked() {return block;}

    virtual ~Protocol() {}

};


#endif