#ifndef COMMUNICATION_HPP
#define COMMUNICATION_HPP

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include <unordered_map>
#include <list>

#include <protocol/protocol.hpp>

// #ifdef __cplusplus
// extern "C" {
// #endif
class Conn {
private:
    int id, num_nodes = -1;
    int num_tasks;

    std::unordered_map<u_int64_t, int> addr_to_id;

    int sockets[16] = {
    -1, -1, -1, -1, 
    -1, -1, -1, -1, 
    -1, -1, -1, -1, 
    -1, -1, -1, -1, 
    };

    int listen_fd;
    struct sockaddr_in self_addr;
    struct sockaddr_in peers_addr[20];

public:
    static long long total, total2;
    Conn(int _id, int _num_peers);

    std::list<Packet> recv();

    void send_packets(std::list<Packet>& packets);

    void connect_all();

    ~Conn();
};

// #ifdef __cplusplus
// }
// #endif

#endif