#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <map>
#include <list>
// #define OMPI_SKIP_MPICXX
// #include "mpi.h"

#include "../protocol/protocol.hpp"
#include "communication.hpp"

// const char* addresses[] = {
//   "10.10.10.1", "10.10.10.4", "10.10.10.5", 
//   "10.10.10.6", "10.10.10.7", "10.10.10.8", 
//   "10.10.10.9", 
// };
const char* addresses[] = {
  "127.0.0.1","127.0.0.1","127.0.0.1",
  "127.0.0.1","127.0.0.1","127.0.0.1",
  "127.0.0.1","127.0.0.1","127.0.0.1",
  "127.0.0.1",
};
const in_port_t ports[] = {
  9821,9822,9823,
  9824,9825,9826,
  9827,9828,9829,
  9830,
};

long long Conn::total = 0;
long long Conn::total2 = 0;
// const char* Conn::get_address(int id) {
//   if(num_peers == -1)
//     return nullptr;
//   return addresses[id%num_peers];
// }

void init_socket(int sock, int reuse) {

  int aux = 1;
  if(reuse)
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (void *) &aux, sizeof(aux) );
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (void *) &aux, sizeof(aux) );
  // aux = 3000;
  // setsockopt(sock, IPPROTO_TCP, TCP_USER_TIMEOUT, (void *) &aux, sizeof(aux) );
}

u_int64_t cast_addr(sockaddr_in addr) {
  u_int64_t result = 0;
  result |= addr.sin_port;
  result |= u_int64_t(addr.sin_addr.s_addr) << 32;
  return result;

}

Conn::Conn(int _id, int _num_nodes) {
  num_nodes = _num_nodes;

  // MPI_Init(NULL, NULL);
  // MPI_Comm_size(MPI_COMM_WORLD, &num_tasks);
  // MPI_Comm_rank(MPI_COMM_WORLD, &id);

  id = _id;;

  if( (listen_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    puts("Listen error");
    exit(-1);
  }

  bzero(&self_addr, sizeof(self_addr));
  self_addr.sin_family = AF_INET;
  self_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  self_addr.sin_port = htons(ports[_id]);

  init_socket(listen_fd, 1);

  if(bind(listen_fd, (struct sockaddr *) &self_addr, sizeof(self_addr)) < 0) {
    puts("Bind error");
    exit(-1);
  }

  if(listen(listen_fd, 20) < 0) {
    puts("Listen error");
    exit(-1);
  } 

}

void Conn::connect_all() {
  for(int i = 0; i < id; i++)
  {
    struct sockaddr_in addr;
    sockets[i] = socket(AF_INET, SOCK_STREAM, 0);

    if(sockets[i] < 0) {
      puts("Socket error");
      exit(-1);
    }

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(ports[i]);

    inet_pton(AF_INET, addresses[i], &addr.sin_addr);

    init_socket(sockets[i], 0);

    while(::connect(sockets[i], (struct sockaddr *) &addr, sizeof(addr)) < 0);

    uint32_t _id = id;
    _id = htonl(_id);
    ::send(sockets[i], &_id, sizeof(_id), 0);


  }
  
  for(int i = id+1; i < num_nodes; i++) {
    int fd;
    struct sockaddr_in addr;
    char addr_string[100];
    socklen_t addr_len = sizeof(addr);

    fd = accept(listen_fd, (struct sockaddr *) &addr, &addr_len);

    if(fd < 0)
    {
      i--;
      continue;
    }

    int _id;
    ::recv(fd, &_id, sizeof(_id), MSG_WAITALL);
    _id = ntohl(_id);

    sockets[_id] = fd;
    
  }
  
  for(int i = 0; i < num_nodes; i++)
    if(i != id)
      fcntl(sockets[i], F_SETFL, fcntl(sockets[i], F_GETFL, 0) | O_NONBLOCK);

}

std::list<Packet> Conn::recv() {
  std::list<Packet> packets;
  Packet packet;

  for(int i = 0; i < num_nodes; i++)
  {
    if(i == id)
      continue;

    int aux = 1;
    while(aux > 0) {
      aux = ::recv(sockets[i], &packet, sizeof(Packet), 0);

      if(aux > 0)
      {
        total2 += aux;
        if(aux != sizeof(Packet))
        {
          puts("Recv error");
          exit(-1);
        }
        packets.push_back(packet);
      }
    }
  }
  return packets;
}

void Conn::send_packets(std::list<Packet>& packets) {
  if(packets.empty())
    return;

  while(not packets.empty())
  {
    Packet p(packets.front());
    
    int aux = ::send(sockets[p.dest], &p, sizeof(Packet), 0);
    if(aux < 0 or aux != sizeof(Packet))
    {
      // printf("%d %d %d\n", p.dest, errno, id);
      puts("Send Error");
      exit(-1);
    }
    total += aux;
    packets.pop_front();
  }
}

Conn::~Conn() {
  // for(int i = 0; i < num_nodes; i++)
  //   if(i != id)
  //     close(sockets[i]);
}
