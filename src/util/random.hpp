#ifndef RANDOM_HPP
#define RANDOM_HPP

#include <algorithm>
#include <stdlib.h>
#include <math.h>

#include <random>
#include <vector>

class Random {
    static Random rand_singleton;
    Random() {

    };
public:
    static std::vector<int> random_subset(std::vector<int> set, int size) {
        std::vector<int> subset(size);

        while(size-- > 0) {
            int i = ::rand() % set.size();
            subset.push_back(set[i]);
            std::swap(set[i], set[set.size()-1]);
            set.pop_back();
        }
        return subset;

    }

    static int randi(int min, int max) {
        return ::rand()%(max-min+1) + min;
    }

    // static Random& get_singleton() {
    //     return rand_singleton;
    // }

};

class Distribution {
protected:
    int min, max, n;
    std::default_random_engine rand_engine;
public:

    Distribution(int min, int max) : min(min), max(max), rand_engine() {
	    n = max - min + 1;
    };

    virtual int get_value() = 0;

};

class UniformDist : public Distribution {
public:
    UniformDist(int min, int max) : Distribution(min, max) {

    }

    int get_value() {
	    return rand()%n + min;
    }

};
class BinomialDist : public Distribution {
protected:
	float p;
	std::binomial_distribution<int> dist;
public:
    BinomialDist(int min, int max, float p) : Distribution(min, max), p(p), dist(n, p) {

    }
    
    int get_value() {
        return dist(rand_engine) + min;
    }

};



#endif
