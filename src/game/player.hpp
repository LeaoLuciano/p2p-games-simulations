#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <stdlib.h>
#include <vector>

#include "game.hpp"
#include "command.hpp"
#include <util/random.hpp>

class Player {
protected:
    int next_time;
    int id;
    Game& game;
    int delay;

    std::vector<int> opponents_ids;
public:
    int time = 0;

    Player(int id, Game& game, int delay) : 
    id(id), game(game), delay(delay),
         next_time(delay), opponents_ids(game.get_num_players()-1) {
        for(int i = 0; i < id; i++)
            opponents_ids[i] = i;
        for(int i = id+1; i < game.get_num_players(); i++)
            opponents_ids[i-1] = i;

    }

    inline int get_id() {
        return id;
    }

    virtual Command new_command(int time) {
        if(time >= next_time)
        {
            next_time = time + game.get_frames_per_round();
            return COMMAND_1;
        }
        return NONE;
    }

    virtual std::vector<int> get_aoi() {
        int aoi_size = Random::randi(0, game.get_num_nodes()-1);
        std::vector<int> players = game.get_players_ids();
        players[id] = players[players.size()];
        players.pop_back();
        return Random::random_subset(players, aoi_size);
    }

    inline std::vector<int> get_opponents_ids() {
        return opponents_ids;
    }

    inline int get_delay() {
        return delay;
    }

    void apply_command(int id, Command command, int round) {
    }

};

class NoPlayer : public Player {

    virtual Command new_command(int time) {
        return NONE;
    }

    virtual std::vector<int> get_aoi() {
        puts("This node isn't a player");
        exit(-1);
    }
};


#endif