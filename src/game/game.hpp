#ifndef GAME_HPP
#define GAME_HPP

#include <vector>

class Game {
protected:
    int num_nodes;
    int num_players;
    int frames_per_round;
    std::vector<int> players_ids;
public:

    Game(int num_nodes, int num_players, int frames_per_round) : 
    num_nodes(num_nodes), num_players(num_players), frames_per_round(frames_per_round),
         players_ids(num_players) {
        for(int i = 0; i < num_players; i++)
            players_ids[i] = i + num_nodes - num_players;
    }

    inline int get_num_nodes() {
        return num_nodes;
    }

    inline int get_num_players() {
        return num_players;
    }

    inline int get_frames_per_round() {
        return frames_per_round;
    }

    inline std::vector<int> get_players_ids() {
        return players_ids;
    }
};


#endif