#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <time.h>
#include <unistd.h>

#define OMPI_SKIP_MPICXX

#include "simulation.hpp"
#include <protocol/protocol.hpp>
#include <protocol/node.hpp>
#include <util/random.hpp>


Random Random::rand_singleton;


int main(int argc, char *argv[])
{
    int num_players, num_nodes, num_rounds;
    
    if(argc > 1)
        num_players = atoi(argv[1]);
    if(argc > 2)
        num_rounds = atoi(argv[2]);
    
    ProtocolFactory protocol_factory(argc, argv);

    if(argc > 3 and strcmp(argv[3], "cs") == 0)
        num_nodes = num_players+1;
    else
        num_nodes = num_players;

    Game game(num_nodes, num_players, 3);
        
    #pragma omp parallel for num_threads(num_nodes)
    for(int i = 0; i < num_nodes; i++)
    {
        Player player(i, game, (i)%3*4+1);
        Protocol *proto = protocol_factory.new_protocol(game, player);
        Node node(game, player, *proto);

        while(proto->get_round() < num_rounds) {
            node.update();
            usleep(1000);
        }

        printf("%d %d\n", node.get_frame(), proto->get_round());
    };
    printf("↑%lld ↓%lld\n", Conn::total, Conn::total2);

    return 0;
}
