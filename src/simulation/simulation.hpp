#ifndef SIMULATION_HPP
#define SIMULATION_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

#include <game/player.hpp>
#include <protocol/protocol.hpp>
#include <protocol/protocols/normal_p2p.hpp>
#include <protocol/protocols/client_server.hpp>
#include <protocol/protocols/lockstep.hpp>
#include <protocol/protocols/async_sync.hpp>
#include <protocol/protocols/pipelined_lockstep.hpp>
#include <protocol/protocols/adaptive_pipeline.hpp>
#include <protocol/protocols/neo.hpp>

// class Simulation {
// private:

// public:
//     struct Config
//     {
//         int num_peers;
//         bool has_server;
//         int num_rounds;
//     };

//     Simulation(Config config);

// };

class ProtocolFactory {

    typedef enum {
        P2P, CS, LOCKSTEP, PIPELINED_LOCKSTEP, ADAPTIVE_PIPELINED_LOCKSTEP,
        NEO,
    } Proto;

    int num_nodes = 0;
    Proto protocol = P2P;
    std::vector<int> args;

        void show_help() {
        printf("Help\n");
        exit(0);
    }

public:
    ProtocolFactory(int argc, char* argv[]) : args() {
        if (argc > 1)
        {
            if (strcmp(argv[1], "help") == 0)
                show_help();
            else
                num_nodes = atoi(argv[1]);
        }

        if (argc > 3) {
            if (strcmp(argv[3], "p2p") == 0)
                protocol = P2P;
            else if (strcmp(argv[3], "cs") == 0)
                protocol = CS;
            else if (strcmp(argv[3], "lockstep") == 0 or strcmp(argv[3], "l") == 0)
                protocol = LOCKSTEP;
            else if (strcmp(argv[3], "pl") == 0)
                protocol = PIPELINED_LOCKSTEP;
            else if (strcmp(argv[3], "apl") == 0)
                protocol = ADAPTIVE_PIPELINED_LOCKSTEP;
            else if (strcmp(argv[3], "neo") == 0)
                protocol = NEO;
            // else
            //     show_help();

        }
        
        for(int i = 4; i < argc; i++) {
            args.push_back(atoi(argv[i]));
        }
    }

    Protocol* new_protocol(Game& game, Player& player) {
        switch (protocol) {
        case CS:
            if (player.get_id() == 0)
                return new Server(game, player);
            return new Client(game, player);
        case LOCKSTEP:
            return new Lockstep(game, player);
        case PIPELINED_LOCKSTEP:
            return new PipelinedLockstep(game, player, args[0]);
        case ADAPTIVE_PIPELINED_LOCKSTEP:
            return new AdaptivePipeline(game, player, args[0]);
        case NEO:
            return new Neo(game, player, args[0]);
        default:
            return new NormalP2P(game, player);

        }
    }


};


#endif