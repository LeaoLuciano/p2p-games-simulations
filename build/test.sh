#!/bin/bash

echo "<------ P2P ------>"
./simulation 10 100 p2p
echo "<------ CS ------>"
./simulation 10 100 cs
echo "<------ Lockstep ------>"
./simulation 10 100 lockstep
echo "<------ Pipelined Lockstep ------>"
./simulation 10 100 pl 5
echo "<------ Adaptive Pipeline ------>"
./simulation 10 100 apl 5
echo "<------ NEO ------>"
./simulation 10 100 neo 5